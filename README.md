# loopback-schedule


## Usage

1. Better to use with memory datasource. So create memory datasource in your `server/datasource.json`
```json
 {
    "memoryDb": {
      "name": "memoryDb",
      "connector": "memory"
    }
 }
 
 
```

2. Configure your `server/model-config.json`:

```json
{
  "_meta": {
    "sources": [
      "loopback/common/models",
      "loopback/server/models",
      "../common/models",
      "./models",
      "loopback-schedule/models"
    ],
    ...
   },
   ...
   "scheduleJob": {
       "dataSource": "memoryDb",
       "public": true
     }
}
```

3. Add boot script `server/boot/agenda.js`

```javascript
module.exports = function (app) {
  app.models.scheduleJob.initJobs([
      {
        name: 'myCustomWorker',
        spec: "*/5 * * * *",
        args: [0, 7],
        callback: (foo, bar) => console.log('hello from schedule! ', foo, bar)
      }
  ]);
};
```



### Endpoints

1. `/scheduleJobs` getting list of your schedule jobs
2. `/scheduleJobs/stop` stopping job by name
3. `/scheduleJobs/run` running stopped job by name
4. `/scheduleJobs/invoke` call job immediately. In response of the job you get object
```javascript
{
  "id": <string> unitque id of invocation,
  "name": <string> name of your job,
  "args": <array> list of your arguments,
  "status": <string> success | error | process,
  "message": <any> return value of your job,
  "start": <string> datetime of start,
  "finish": <string> datetime of finish
}
```
5. If your job has error or process status you could get job info by invocation ID
by route `scheduleJobs/getInvokedJobStatus`
