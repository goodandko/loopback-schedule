'use strict';

const schedule = require('node-schedule');
const moment = require('moment');
const uuidv4 = require('uuid/v4');

const STATUS_ACTIVE = 'active';
const STATUS_STOPPED = 'stopped';

const INVOKE_STATUS_PROCESS = 'process';
const INVOKE_STATUS_ERROR = 'error';
const INVOKE_STATUS_SUCCESS = 'success';

let JOB_LIST = [];

const INVOKE_JOB_INFO_MAP = new Map();

module.exports = function (scheduleJob) {


  scheduleJob.initJobs = function (jobList) {
    console.log('Starting scheduling');

    jobList.push({
      name: 'scheduleJob.cleanInvocations',
      spec: "*/10 * * * *",
      args: [-10, 'minutes'],
      callback: scheduleJob.cleanInvocations.bind(scheduleJob)
    });

    Promise.all(jobList.map(jobItem => this.setupJob(jobItem)))
      .then(() => console.log("Schedule jobs has been successfully inited"))
      .catch(e => console.log(e.message));
  };

  /**
   *
   * @param jobConfig
   * @returns {Promise<*>}
   */
  scheduleJob.setupJob = async function (jobConfig) {
    const {name, spec, args, callback} = jobConfig;

    if (await this.findOne({where: {name}})) {
      throw new Error(`Job ${name} has already been setuped`);
    }

    schedule.scheduleJob(name, spec, () => callback(...args));

    JOB_LIST.push({name, spec, args, callback});

    return await this.create({
      name,
      spec,
      args,
      status: STATUS_ACTIVE,
      createdAt: moment.utc()
    })
  };

  /**
   *
   * @param name
   * @returns {Promise<*>}
   */
  scheduleJob.deleteJob = async function (name) {
    const job = await this.findOne({where: {name}});

    if (!job) {
      throw new Error(`Job ${name} not exists`);
    }

    schedule.cancelJob(job.name);

    JOB_LIST = JOB_LIST.filter(jobItem => jobItem.name !== name) || null;

    return await job.delete();
  };

  /**
   *
   * @param name
   * @returns {Promise<void>}
   */
  scheduleJob.stop = async function (name) {
    const job = await this.findOne({where: {name}});

    if (!job) {
      throw new Error(`Job ${name} not exists`);
    }

    if (job.status === STATUS_STOPPED) {
      throw new Error(`Job ${name} already stopped`);
    }

    schedule.cancelJob(job.name);

    return await job.updateAttribute('status', STATUS_STOPPED);
  };

  scheduleJob.remoteMethod('stop', {
    description: 'running job by name.',
    isStatic: true,
    accepts: [
      {arg: 'name', type: 'string', required: 'true'},
    ],
    returns: {type: 'object', root: true}, http: {verb: 'get'}
  });

  /**
   *
   * @param name
   * @returns {Promise<void>}
   */
  scheduleJob.run = async function (name) {
    const job = await this.findOne({where: {name}});

    if (!job) {
      throw new Error(`Job ${name} not exists`);
    }

    if (job.status === STATUS_ACTIVE) {
      throw new Error(`Job ${name} already ran`);
    }

    const jobItem = JOB_LIST.find(jobItem => jobItem.name === name) || null;

    if (!jobItem) {
      throw new Error(`Callback not found`);
    }

    schedule.scheduleJob(job.name, job.spec, () => jobItem.callback());

    return await job.updateAttribute('status', STATUS_ACTIVE);
  };

  scheduleJob.remoteMethod('run', {
    description: 'running job by name.',
    accepts: [
      {arg: 'name', type: 'string', required: 'true'},
    ],
    returns: {type: 'object', root: true}, http: {verb: 'get'}
  });

  /**
   *
   * @param name
   * @returns {Promise<{name: *, status: string}>}
   */
  scheduleJob.invoke = async function (name, args = null) {
    const job = await this.findOne({where: {name}});

    if (!job) {
      throw new Error(`Job ${name} not exists`);
    }

    if (job.status === STATUS_STOPPED) {
      throw new Error(`Job ${name} has stopped. To Invoke start job`);
    }

    const jobItem = JOB_LIST.find(jobItem => jobItem.name === name) || null;

    if (!jobItem) {
      throw new Error(`Job ${name} cannot be invoked`);
    }

    args = args || job.args;

    const jobInfo = {
      id: uuidv4(),
      name,
      args,
      status: INVOKE_STATUS_PROCESS,
      message: null,
      start: moment.utc(),
      finish: null
    };

    INVOKE_JOB_INFO_MAP.set(jobInfo.id, jobInfo);

    Promise.resolve(jobItem.callback(...args))
      .then((result = null) => {
        if (INVOKE_JOB_INFO_MAP.has(jobInfo.id)) {
          const savedJobInfo = INVOKE_JOB_INFO_MAP.get(jobInfo.id);

          savedJobInfo.status = INVOKE_STATUS_SUCCESS;
          savedJobInfo.message = result;
          savedJobInfo.finish = moment.utc();
        }
      })
      .catch(err => {
        if (INVOKE_JOB_INFO_MAP.has(jobInfo.id)) {
          const savedJobInfo = INVOKE_JOB_INFO_MAP.get(jobInfo.id);

          savedJobInfo.status = INVOKE_STATUS_ERROR;
          savedJobInfo.message = err.message;
          savedJobInfo.finish = moment.utc();
        }
      });

    return jobInfo;
  };

  scheduleJob.remoteMethod('invoke', {
    description: 'invoking job by name.',
    accepts: [
      {arg: 'name', type: 'string', required: 'true'},
      {arg: 'args', type: 'object'}
    ],
    returns: {type: 'object', root: true}, http: {verb: 'get'}
  });

  scheduleJob.getInvokedJobStatus = async function (id) {
    if (!INVOKE_JOB_INFO_MAP.has(id)) {
      throw new Error(`Invokation with id ${id} not exists`);
    }

    return INVOKE_JOB_INFO_MAP.get(id);
  };

  scheduleJob.remoteMethod('getInvokedJobStatus', {
    description: 'get status of invoked jobs.',
    accepts: [
      {arg: 'id', type: 'string', required: 'true'},
    ],
    returns: {type: 'object', root: true}, http: {verb: 'get'}
  });


  scheduleJob.cleanInvocations = async function (offset = -1, offsetType = 'hour') {
    const types = ['hour', 'hours', 'day', 'days', 'minute', 'minutes'];

    if (!types.includes(offsetType)) {
      throw new Error('Invalid offset type');
    }

    const offsetDate = moment.utc().add(offset, offsetType);

    const keysToRemove = new Set();

    for (const [uid, jobInfo] of INVOKE_JOB_INFO_MAP) {
      if (jobInfo.start < offsetDate) {
        keysToRemove.add(uid);
      }
    }

    for (const key of keysToRemove) {
      INVOKE_JOB_INFO_MAP.delete(key);
    }
  };

  scheduleJob.disableRemoteMethodByName("create", true);
  scheduleJob.disableRemoteMethodByName("upsert", true);
  scheduleJob.disableRemoteMethodByName("updateAll", true);
  scheduleJob.disableRemoteMethodByName("updateAttributes", true);
  scheduleJob.disableRemoteMethodByName("replaceOrCreate", true);
  scheduleJob.disableRemoteMethodByName("upsertWithWhere", true);
  scheduleJob.disableRemoteMethodByName("replaceById", true);
  scheduleJob.disableRemoteMethodByName("createChangeStream", true);
  scheduleJob.disableRemoteMethodByName("findById", true);
  scheduleJob.disableRemoteMethodByName("findOne", true);
  scheduleJob.disableRemoteMethodByName("deleteById", true);
  scheduleJob.disableRemoteMethodByName("confirm", true);
  scheduleJob.disableRemoteMethodByName("count", true);
  scheduleJob.disableRemoteMethodByName("exists", true);
  scheduleJob.disableRemoteMethodByName('__get__tags', true);
  scheduleJob.disableRemoteMethodByName('__create__tags', true);

};

